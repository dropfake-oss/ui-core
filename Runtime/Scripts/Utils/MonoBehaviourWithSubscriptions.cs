using Container.Utils;
using System;
using UnityEngine;

namespace UI.Core
{
	public class MonoBehaviourWithSubscriptions : MonoBehaviour, IDisposable, IWithSubscriptions
	{
		protected DisposableCollection<IDisposable> Subs = new DisposableCollection<IDisposable>();

		public MonoBehaviourWithSubscriptions()
		{
		}

		protected virtual void OnDestroy()
		{
			this.Dispose();
		}

		public virtual void Dispose()
		{
			this.ClearSubscriptions();
		}

		public void ClearSubscriptions()
		{
			this.Subs.Dispose();
		}
	}
}
