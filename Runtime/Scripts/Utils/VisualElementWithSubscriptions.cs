using System;
using UnityEngine;
using UnityEngine.UIElements;
using Container.Utils;

namespace UI.Core
{
	public class VisualElementWithSubscriptions : VisualElement, IDisposable, IWithSubscriptions
	{
		protected DisposableCollection Subs = new DisposableCollection();
		protected bool IsInitialized = false;

		public VisualElementWithSubscriptions()
		{
			this.RegisterCallback<GeometryChangedEvent>(OnVisualElementWithSubscriptionsInit);
			this.RegisterCallback<DetachFromPanelEvent>(OnVisualElementWithSubscriptionsDestory);
		}

		protected virtual void OnVisualElementWithSubscriptionsInit(GeometryChangedEvent evt)
		{
			this.Awake();
			this.IsInitialized = true;
			this.UnregisterCallback<GeometryChangedEvent>(OnVisualElementWithSubscriptionsInit);
		}

		protected virtual void OnVisualElementWithSubscriptionsDestory(DetachFromPanelEvent evt)
		{
			this.OnDestroy();
			this.IsInitialized = false;
			this.Dispose();
			this.UnregisterCallback<DetachFromPanelEvent>(OnVisualElementWithSubscriptionsDestory);
		}

		public virtual void Awake()
		{
		}

		public virtual void OnDestroy()
		{
		}

		public virtual void Dispose()
		{
			this.ClearSubscriptions();
		}

		public void ClearSubscriptions()
		{
			this.Subs.Dispose();
		}

		public void AddSubscription(IDisposable sub)
		{
			this.Subs.Add(sub);
		}
	}
}
