using System;
using UnityEngine.UIElements;
using UnityEngine;
using UniRx;

namespace UI.Core
{
	public class VisualElementWithHover : VisualElementWithSubscriptions
	{
		public TimeSpan Debounce = TimeSpan.FromMilliseconds(300);
		private BehaviorSubject<bool> MouseOver = new BehaviorSubject<bool>(false);


		private bool Hovering = false;
		public IObservable<bool> IsHovering => this.MouseOver.AsObservable().Throttle(this.Debounce).Where(mouseOver =>
		{
			if (mouseOver == this.Hovering)
			{
				return false;
			}

			this.Hovering = mouseOver;
			return true;
		});

		public VisualElementWithHover()
		{
		}

		protected override void OnVisualElementWithSubscriptionsInit(GeometryChangedEvent evt)
		{
			base.OnVisualElementWithSubscriptionsInit(evt);
			this.RegisterCallback<MouseEnterEvent>(e =>
			{
				this.MouseOver.OnNext(true);
			});
			this.RegisterCallback<MouseLeaveEvent>(e =>
			{
				this.MouseOver.OnNext(false);
			});
		}

		protected override void OnVisualElementWithSubscriptionsDestory(DetachFromPanelEvent evt)
		{
			this.MouseOver.OnNext(false);
			base.OnVisualElementWithSubscriptionsDestory(evt);
		}
	}
}
